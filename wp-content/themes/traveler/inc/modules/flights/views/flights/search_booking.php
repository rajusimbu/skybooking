<?php 
$return_date = STInput::get('end');
$depart_date = STInput::get('start');
$origin = STInput::get('origin');
$origin_name = STInput::get('origin_name');
$destination = STInput::get('destination');
$destination_name = STInput::get('destination_name');
$passenger = STInput::get('passenger');
$cabin_class = STInput::get('cabin_class');
$flight_type = STInput::get('flight_type');

$flight_search_fields = st()->get_option('flight_search_fields','');
$search_result_page = '';
if(!empty($flight_search_fields) && is_array($flight_search_fields)) {
    $search_result_page = st()->get_option('flight_search_result_page', '');
    if (!empty($search_result_page)) {
        $search_result_page = get_the_permalink($search_result_page);
    }else{
		
	}
}
$flight_detail = 'http://localhost/skybooking/flightdetails/';
?>
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="col-sm-12 flight_content"><span>Not available</span></div>
<ul><div class="col-sm-12 flight_description"><li><span>There will be no search results if no flights are scheduled on the selected dates.</span></li></div>
<div class="col-sm-12 flight_description"><li><span>Fares may be unavailable in the selected class, but available in other travel classes..</span></li></div>
<div class="col-sm-12 flight_description"><li><span>Flights may also be fully booked..</span></li></div></ul>
</div>
<div class="col-md-12" style="margin-top:10px;">
<div class="col-sm-12 form-button">
<form action="<?php echo esc_url($flight_detail); ?>" method="get">
<input type="hidden" name="origin" value="<?php echo $origin;?>">
<input type="hidden" name="origin_name" value="<?php echo $origin_name;?>">
<input type="hidden" name="destination_name" value="<?php echo $destination_name;?>">
<input type="hidden" name="destination" value="<?php echo $destination;?>">
<input type="hidden" name="start" value="<?php echo $depart_date;?>">
<input type="hidden" name="end" value="<?php echo $return_date;?>">
<input type="hidden" name="passenger" value="<?php echo $passenger;?>">
<input type="hidden" name="cabin_class" value="<?php echo $cabin_class;?>">
<input type="hidden" name="flight_type" value="<?php echo $flight_type;?>">
<div class="col-sm-3 search-flight"><button class="btn_ctnue" type="submit" id="continue_search">Continue</button></div>
</form>
</div>
</div>
</div>
</div>