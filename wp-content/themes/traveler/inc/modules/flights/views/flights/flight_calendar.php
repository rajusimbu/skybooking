 <style type="text/css" media="screen,print">
  table {
   border: 1px solid #999;
   border-collapse: collapse;
   /* font-family: Georgia, Times, serif; */
   }
   th {
   border: 1px solid #999;
   font-size: 70%;
   text-transform: uppercase;
   }
   td {
   border: 1px solid #999;
   height: 5em;
   width:5em;
   padding: 5px;
   vertical-align: top;
   }
   td .depart{
	   float: left;
       margin-top: 35px;
   }
   td .return{
	   float: right;
   }
   td:hover {
		border-style: double;
		border-color: #001d48;
	}
   caption {
   font-size: 300%;
   font-style: italic;
   }
   .day {
   text-align: center;
   margin-top: 10px;
   }
   .notes {
   font-family: Arial, Helvetica, sans-serif;
   font-size: 80%;
   text-align: right;
   padding-left: 20px;
   }
   .birthday {
   background-color: #ECE;
   }
   .weekend {
   background-color: #F3F3F3;
   }
   .wrap{
	   background: #001d48;
   }
  </style>
  <div class="container">
  <table>
  <!--<caption>Flight Calendar</caption>-->
  <tbody>
    <tr>
      <td><div class="depart"><span>Departing</span></div>
	  <div class="return"><span>Returning</span></div></td>
      <td class="wrap"><div class="day">Fri 05 Jan</div></td>
      <td class="wrap"><div class="day">Sat 06 Jan</div></td>
      <td class="wrap"><div class="day">Sun 07 Jan</div></td>
      <td class="wrap"><div class="day">Mon 08 Jan</div></td>
      <td class="wrap"><div class="day">Tue 09 Jan</div></td>
      <td class="wrap"><div class="day">Wed 10 Jan</div></td>
    </tr>
    <tr>
      <td class="wrap">
	  <div class="day">Fri 05 Jan</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
    </tr>
    <tr>
      <td class="wrap"><div class="day">Sat 06 Jan</div></td>
      <td><div class="day">USD 20</div></td>
      <td><div class="day">USD 30</div></td>
      <td><div class="day">USD 40</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">USD 30</div></td>
    </tr>
    <tr>
      <td class="wrap"><div class="day">Sun 07 Jan</div></td>
      <td><div class="day">USD 20</div></td>
      <td><div class="day">USD 40</div></td>
      <td><div class="day">USD 60</div></td>
      <td><div class="day">USD 50</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
    </tr>
     <tr>
      <td class="wrap"><div class="day">Mon 08 Jan</div></td>
      <td><div class="day">USD 20</div></td>
      <td><div class="day">USD 30</div></td>
      <td><div class="day">USD 40</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">USD 30</div></td>
    </tr>
	 <tr>
      <td class="wrap"><div class="day">Tue 09 Jan</div></td>
      <td><div class="day">USD 20</div></td>
      <td><div class="day">USD 40</div></td>
      <td><div class="day">USD 60</div></td>
      <td><div class="day">USD 50</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
    </tr>
	 <tr>
     <td class="wrap"><div class="day">Wed 10 Jan</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
    </tr>
	 <tr>
      <td class="wrap"><div class="day">Wed 10 Jan</div></td>
	  <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">USD 10</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
      <td><div class="day">Not Available</div></td>
    </tr>
  </tbody>
</table>
</div>
<?php
    echo st_flight_load_view('flights/search_booking');
 ?>
 