<?php
/**
 * Created by wpbooking.
 * Developer: nasanji
 * Date: 6/14/2017
 * Version: 1.0
 */
extract($data);
if(!isset($field_size)) $field_size='lg';
$title = st_the_language('cabin_class');
?>
<div class="form-group form-group-select-plus flight-input form-group-<?php echo esc_attr($field_size)?>">
    <label><?php echo $title;?></label>
    <select id="cabin_class" class="form-control" name="cabin_class">
        <option value="Economy class">Economy class</option>
        <option value="Business class">Business class</option>
        <option value="First class">First class</option>
    </select>
</div>
