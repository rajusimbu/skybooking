<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * function
 *
 * Created by ShineTheme
 *
 */

if(!defined('ST_TEXTDOMAIN'))
define ('ST_TEXTDOMAIN','traveler');

if(!defined('ST_TRAVELER_VERSION'))
{
    $theme=wp_get_theme();
	if($theme->parent())
	{
		$theme=$theme->parent();
	}
    define ('ST_TRAVELER_VERSION',$theme->get( 'Version' ));
}


$status=load_theme_textdomain(ST_TEXTDOMAIN,get_stylesheet_directory().'/language');

get_template_part('inc/class.traveler');

require_once('wp_bootstrap_navwalker.php');

//Disable Admin Bar for normal user
add_filter( 'show_admin_bar', '__return_false', PHP_INT_MAX );

add_action( 'pre_user_query', function( $uqi ) {
    global $wpdb;
 
    $search = '';
    if ( isset( $uqi->query_vars['search'] ) )
        $search = trim( $uqi->query_vars['search'] );
      
    if ( $search ) {
        $search = trim($search, '*');
        $the_search = '%'.$search.'%';

        $search_meta = $wpdb->prepare("
        ID IN ( SELECT user_id FROM {$wpdb->usermeta}
        WHERE ( ( meta_key='sky_user_code' )
            AND {$wpdb->usermeta}.meta_value LIKE '%s' )
        )", $the_search);
 
        $uqi->query_where = str_replace(
            'WHERE 1=1 AND (',
            "WHERE 1=1 AND (" . $search_meta . " OR ",
            $uqi->query_where );
    }
});

add_action('wp_ajax_airport_search' , 'airport_search');

function airport_search(){
	global $wpdb;
	$name = $_GET['name'];
	$table = $wpdb->prefix.'st_flight_airport';
	$table1 = $wpdb->prefix.'st_location_nested';
	$sql  = "SELECT ln.*,fa.airport_id,fa.iata_id FROM ".$table." as fa left join ".$table1." as ln on ln.location_id = fa.location where ln.name like '%".$name."%'";
	$results = $wpdb->get_results( $sql );
	return json_encode($results);
}

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );