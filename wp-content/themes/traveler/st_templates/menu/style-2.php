<nav class="navbar navbar-default primary-nav">
  <div class="container"> <!-- full screen use container-fluid --!>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url('/'); ?>">
        <?php //echo strtoupper( get_bloginfo('name') ) ?>
        <img src="https://skybooking.net/wp-content/uploads/2017/11/skybooking-logo-landscape@2x.png" height="20px" style="height:20px !important" >    
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">     

        <?php if(has_nav_menu('primary')){
                wp_nav_menu(
                  array('theme_location'=>'primary',
                        "depth" => 2,                        
                        'container' => false,
                        'menu_class' => 'nav navbar-nav',                      
                        'walker' => new wp_bootstrap_navwalker()
                ));
              }
    ?>
     
    <?php get_template_part('users/user','nav');?>
    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>