<?php
/*
Template Name: Register Full
*/

/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Template Name : Register Form
 *
 * Created by ShineTheme
 *
 */
get_header("full");


$reset = 'false';
if(!empty($_REQUEST['btn_reg'])){
    $reset = STUser_f::registration_user();
}

$class_form = "";
if(is_page_template('template-login.php')){
    $class_form = 'form-group-ghost';
}

$btn_register = get_post_meta(get_the_ID(),'btn_register',true);
if(empty($btn_register))$btn_register=__("Register",ST_TEXTDOMAIN);


wp_enqueue_style( 'intlTelInput.css' ,  get_template_directory_uri().'/css/intlTelInput.css' );
wp_enqueue_script('bootstrap-datepicker.js');
wp_enqueue_script( 'google-location.js', get_template_directory_uri().'/js/google-location.js' );
wp_enqueue_script( 'intlTelInput.min.js' , get_template_directory_uri().'/js/intlTelInput.min.js' );
wp_enqueue_script( 'utils.js' , get_template_directory_uri().'/js/utils.js' );
?>


<style type="text/css">
    .register-full{       
        background-color: rgba(0,0,0,0.8);
    }
    .register-full .info-box, .register-full .register-box{
        box-sizing: border-box;
        width: 50%;
        float: left;
        display: block;
        margin : 0px !important;
        min-height: 500px;
        padding: 50px;
    }
    .register-page-title{
        font-size: 36px;        
        margin-bottom: 40px !important;
    }
    .register-page{        
        background-color: rgba(0,112,140,0.5);       
    }

    .register-page .col-md-6, .register-page .col-md-12, .register-page .col-md-4 {
        padding-right: 10px !important;
        padding-left: 10px !important;
    }

    .iti-flag {background-image: url("path/to/flags.png");}

    .intl-tel-input{
        width: 100% !important;
    }
    ul.country-list{
        z-index: 99992 !important;       
    }
    ul.country-list .country-name{
        color: #555;
    }



    @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
      .iti-flag {background-image: url("path/to/flags@2x.png");}
    }

    .js .inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
    }

    .inputfile + label {
        max-width: 100%;
        font-size: 1em;            
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        padding: 5px;      
    }

    .no-js .inputfile + label {
        display: none;
    }

    .inputfile:focus + label,
    .inputfile.has-focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .inputfile + label * {
        /* pointer-events: none; */
        /* in case of FastClick lib use */
    }

    .inputfile-6 + label {
        color: #ccc;
        width: 100%;
        display: inline-block;
    }

    .inputfile-6 + label {
        border: 1px solid #fff;
        background-color: #fff;
        padding: 0;
    }

    .inputfile-6:focus + label,
    .inputfile-6.has-focus + label,
    .inputfile-6 + label:hover {
        border-color: #fff;
    }

    .inputfile-6 + label span,
    .inputfile-6 + label strong {
        padding: 5px;
        /* 10px 20px */
    }

    .inputfile-6 + label span {
        width: 65%;       
        display: inline-block;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        vertical-align: top;
        color : #555;
    }

    .inputfile-6 + label strong {
        width: 35%;
        height: 100%;
        font-weight: normal;
        color: #f1e5e6;
        background-color: #00458c;
        text-align: center;
        display: inline-block;
    }

    .inputfile-6:focus + label strong,
    .inputfile-6.has-focus + label strong,
    .inputfile-6 + label:hover strong {
        background-color: #043163;
    }
    .checkbox label{
        padding-top: 10px;
    }
    .st_check_term_conditions a{
        text-decoration: underline;
        font-weight: bold;
    }

    form.register-form input[type=submit]{
        border: 1px solid #FFF;
        margin-top: 14px;
    }
    @media screen and (max-width: 50em) {
        .inputfile-6 + label strong {
            display: block;
        }
    }
</style>
<div class="login full-center">
    <div class="container">     
        <div class="row">
            <div class="col-xs-12 ">
                <h2 class="text-center register-page-title">BE A PART OF <a href="<?php echo home_url() ?>" title="<?php bloginfo('name')?> - <?php bloginfo('description')?>"><strong>SKY BOOKING</strong></a></h2>
            </div>
        </div>
        <div class="row row-wrap register-full">            
            <div class="info-box hidden-sm hidden-xs">
                <?php
                    while(have_posts()){
                        the_post();
                        the_content();
                    }
                ?>
            </div>            
            <div class="register-box register-page" >
                <form class="register-form" action="" method="POST" novalidate>
                    <h4 class="text-center"><strong>USER INFORMATION</strong></h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-user input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="First Name" name="firstName">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-user input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="Last Name" name="lastName"> 
                            </div>
                        </div>                   
                        <div class="col-md-6">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-envelope input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="E-Mail" name="email">                      
                            </div>
                        </div>
                        <div class="col-md-6">    
                            <div class="form-group form-group-icon-left">                              
                                <input class="form-control" type="tel" id="phone">                      
                            </div>
                        </div>
                        <div class="col-md-6">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-venus-mars input-icon input-icon-highlight"></i>
                                <select class="form-control" name="gender">
                                    <option value="" disabled selected>Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div data-date-format="<?php echo TravelHelper::getDateFormatJs(); ?>" data-provide="datepicker" class="form-group input-daterange form-group-icon-left">                               
                                <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                <input id="dob" placeholder="Birthday <?php echo TravelHelper::getDateFormatJs(); ?>" class="form-control" name="dob" type="text" name="dob" />
                            </div> 
                        </div>  
                        <div class="col-md-12">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-location-arrow input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="Address Line 1" name="address1">                      
                            </div>
                        </div> 
                        <div class="col-md-12">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-location-arrow input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="Address Line 2" name="address2">                      
                            </div>
                        </div>
                        <div class="col-md-4">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-location-arrow input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="City" id="city" name="city">
                            </div>
                        </div>
                        <div class="col-md-4">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-location-arrow input-icon input-icon-highlight"></i>
                                <input class="form-control" type="text" placeholder="Country" id="country" name="country">
                            </div>
                        </div> 
                        <div class="col-md-4">    
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-location-arrow input-icon"></i>
                                <input class="form-control" type="text" placeholder="Area Code" name="areaCode">
                            </div>
                        </div>
                        <div class="col-md-12">                                                   
                            <!-- <input type="file" name="passport" id="passport" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple /> -->
                            <input type="file" name="passport" id="passport" class="inputfile inputfile-6"/>
                            <label for="passport"><strong>Upload Passport</strong> <span></span> </label>
                        </div>
                        <div class="col-md-12">                                                   
                            <input type="file" name="license" id="license" class="inputfile inputfile-6"/>
                            <label for="license"><strong>Company License</strong> <span></span> </label>
                        </div>
                        <div class="checkbox st_check_term_conditions col-md-9">
                            <label>
                                <input class="i-check term_condition" name="term_condition" type="checkbox" /><?php echo st_get_language('i_have_read_and_accept_the').' <a target="_blank" href="'.get_the_permalink(st()->get_option('page_terms_conditions')).'">'.st_get_language('terms_and_conditions').'</a>';?>
                            </label>
                            <label>
                                <input class="i-check email_subscription" name="email_subscription" type="checkbox" <?php if(STInput::post('email_subscription')==1) echo 'checked'; ?>/> Send me the promotions and special deal
                            </label>
                        </div>
                        <div class="col-md-3">
                            <input type="submit" name="btn_reg" value="Register" class="btn btn-primary btn-lg btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-center mt50">© <?php echo date('Y') ?> <?php  strtoupper(bloginfo('name')) ?>. All Right Reserved | By : PathFinder Co,. LTD</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">   
    'use strict';

;( function( $, window, document, undefined )
{
    $( '.inputfile' ).each( function()
    {
        var $input   = $( this ),
            $label   = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e )
        {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $label.find( 'span' ).html( fileName );
            else
                $label.html( labelVal );
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });

   

})( jQuery, window, document );
</script>
<?php  get_footer('full'); ?>